#use manifestacija;

#drop table if exists km;
#drop table if exists grad;

drop database if exists manifestacija;

create database manifestacija;

use manifestacija;

create table grad (
	grad_id int not null auto_increment,
	naziv varchar(20) not null,
    ptt int not null,
    primary key (grad_id)
);

create table km (
	km_id int not null auto_increment,
    naziv varchar(50),
    broj_posetilaca int not null,
    grad_id int not null,
    primary key (km_id),
    foreign key (grad_id) references grad(grad_id) on delete restrict
    );
    
insert into grad (naziv, ptt) values ('Bečej', 21220);
insert into grad (naziv, ptt) values ('Novi Sad', 21000);
insert into grad (naziv, ptt) values ('Beograd', 11000);
insert into grad (naziv, ptt) values ('Subotica', 24000);
insert into grad (naziv, ptt) values ('Zrenjanin', 23000);
insert into grad (naziv, ptt) values ('Niš', 18000);

insert into km (naziv, broj_posetilaca, grad_id) values ('Majske igre 1988', 10000, 1);
insert into km (naziv, broj_posetilaca, grad_id) values ('Exit 2010', 85000, 2);
insert into km (naziv, broj_posetilaca, grad_id) values ('Bemus', 15000, 3);
insert into km (naziv, broj_posetilaca, grad_id) values ('Exit 2016', 121355, 2);
insert into km (naziv, broj_posetilaca, grad_id) values ('Dani piva u ZR', 38567, 5);