package model;

public class Grad {

	protected int grad_id;
	protected String naziv;
	protected int ptt;

	public Grad() {
		super();
	}

	public Grad(int grad_id, String naziv, int ptt) {
		this.grad_id = grad_id;
		this.naziv = naziv;
		this.ptt = ptt;
	}

	@Override
	public String toString() {
		return "Grad " + naziv + " ima id " + grad_id + " i ptt broj " + ptt;
	}
	
	public String toStringKM() {
		return naziv;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (grad_id != other.grad_id)
			return false;
		return true;
	}

	public int getGrad_id() {
		return grad_id;
	}

	public void setGrad_id(int grad_id) {
		this.grad_id = grad_id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getPtt() {
		return ptt;
	}

	public void setPtt(int ptt) {
		this.ptt = ptt;
	}

}
