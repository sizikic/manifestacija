package model;

public class Km {

	protected int km_id;
	protected String naziv;
	protected int brojPosetilaca;
	protected Grad grad;

	public Km() {
	}

	public Km(int km_id, String naziv, int brojPosetilaca, Grad grad) {
		this.km_id = km_id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
		this.grad = grad;
	}
	
	public Km(String tekst) {
		String[] tokeni = tekst.split(",");
		this.naziv = tokeni[0];
		this.brojPosetilaca = Integer.parseInt(tokeni[1]);
	}

	@Override
	public String toString() {
		return "Kulturna Manifestacija sa nazivom " + naziv + " je imala " + brojPosetilaca + " posetilaca, a odrzana je u gradu " + grad.toStringKM() + ".";
	}
	
	public String toFileIzvestaj() {
		return naziv + "," + brojPosetilaca;
	}

	public int getKm_id() {
		return km_id;
	}

	public void setKm_id(int km_id) {
		this.km_id = km_id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}

	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}

}
