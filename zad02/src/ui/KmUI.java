package ui;

import java.util.List;
import dao.KmDAO;
import model.Grad;
import model.Km;
import utils.ScannerWrapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

public class KmUI {

	public static void menu() {

		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveKm(ApplicationUI.conn);
				break;
			case 2:
				unesiKm();
				break;
			case 3:
				izmeniKm();
				break;
			case 4:
				obrisiKm();
				break;
			case 5:
				izvestajPosetiociMax();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa kulturnim manifestacijama - opcije:");
		System.out.println("\tOpcija broj 1 - ispis podataka o svim kulturnim manifestacijama");
		System.out.println("\tOpcija broj 2 - unos nove kulturne manifestacije");
		System.out.println("\tOpcija broj 3 - izmena postojece kulturne manifestacije");
		System.out.println("\tOpcija broj 4 - brisanje kulturne manifestacije");
		System.out.println("\tOpcija broj 5 - izvestaj o manifestaciji sa najvećim brojem posetilaca");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	protected static List<Km> ispisiSveKm(Connection conn) {
		List<Km> listaKm = KmDAO.getAll(ApplicationUI.conn);
		for (Km km : listaKm) {
			System.out.println(km);
		}
		return listaKm;
	}

	protected static Km pronadjiKmId(int id) {
		Km retVal = KmDAO.getKmById(id, ApplicationUI.conn);
		return retVal;
	}

	protected static Km pronadjiKm() {
		System.out.println("Unesite naziv manifestacije:");
		String naziv = ScannerWrapper.ocitajTekst();
		Km retVal = KmDAO.getKmByName(naziv, ApplicationUI.conn);
		return retVal;
	}

	protected static boolean unesiKm() {
		System.out.println("Unesi naziv nove manifestacije:");
		String naziv = ScannerWrapper.ocitajTekst();
		Km km = KmDAO.getKmByName(naziv, ApplicationUI.conn);
		while (km != null) {
			System.out.println("Manifestacija već postoji u evidenciji!");
			System.out.println("Unesi naziv nove manifestacije:");
			naziv = ScannerWrapper.ocitajTekst();
			km = KmDAO.getKmByName(naziv, ApplicationUI.conn);
		}
		System.out.println("Unesi broj posetilaca:");
		int posetioci = ScannerWrapper.ocitajCeoBroj();
		Grad g = GradUI.pronadjiGrad();
		km = new Km(0, naziv, posetioci, g);
		boolean status = KmDAO.add(ApplicationUI.conn, km);
		if (status) {
			System.out.println("Manifestacija je uspešno dodata.");
		}
		return status;
	}

	protected static Km izmeniKm() {
		Km km = pronadjiKm();
		while (km == null) {
			System.out.println("Manifestacija sa unetim nazivom ne postoji u evidenciji!");
			km = pronadjiKm();
		}
		System.out.println("Unesi nov naziv manifestacije:");
		String novNaziv = ScannerWrapper.ocitajTekst();
		km.setNaziv(novNaziv);
		System.out.println("Unesi broj posetilaca:");
		int posetioci = ScannerWrapper.ocitajCeoBroj();
		km.setBrojPosetilaca(posetioci);
		Grad g = GradUI.pronadjiGrad();
		km.setGrad(g);
		KmDAO.update(ApplicationUI.conn, km);
		return km;
	}

	protected static void obrisiKm() {
		Km km = pronadjiKm();
		boolean status = false;
		if (km != null) {
			status = KmDAO.delete(ApplicationUI.conn, km.getKm_id());
		}

		if (status) {
			System.out.println("Kulturna manifestacija je uspešno obrisana!");
		}
	}

	protected static void izvestajPosetiociMax() {
		String sP = System.getProperty("file.separator");
		File izvestaj = new File("." + sP + "izvestaj.txt");
		Km km = KmDAO.pronadjiKmMax(ApplicationUI.conn);
		try {
			pisiUFajlIzvestaj(izvestaj, km);
		}catch (Exception e) {
			System.out.println("Greška pri pisanju fajla!");
		}
		Km procitanKm = null;
		try {
			procitanKm = citajIzvestajIzFajla(izvestaj);
		} catch (Exception e ) {
			System.out.println("Greška pri čitanju izveštaja!");
		}
		System.out.println("Kulturna manifestacija sa najvećim brojem poseta pod nazivom " + procitanKm.getNaziv()
				+ " sa brojem posecilaca " + procitanKm.getBrojPosetilaca() + ".");
	}

	protected static Km citajIzvestajIzFajla(File dokument) throws IOException {
		Km km = null;
		if (dokument.exists()) {
			BufferedReader in = new BufferedReader(new FileReader(dokument));
			in.mark(1);
			if (in.read() != '\ufeff') {
				in.reset();
			}

			String s2;
			while ((s2 = in.readLine()) != null) {
				km = new Km(s2);
			}
			in.close();
			
		} else {
			System.out.println("Ne postoji fajl!");
		}
		return km;
	}

	protected static void pisiUFajlIzvestaj(File dokument, Km km) throws IOException {
		PrintWriter out2 = new PrintWriter(new FileWriter(dokument));
		out2.println(km.toFileIzvestaj());
		out2.flush();
		out2.close();
	}

}
