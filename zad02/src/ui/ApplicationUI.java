package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import utils.ScannerWrapper;

public class ApplicationUI {

	public static Connection conn;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/manifestacija", "root", "root");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();

			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();

			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				GradUI.menu();
				break;
			case 2:
				 KmUI.menu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Kulturna manifestacija - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa gradovima");
		System.out.println("\tOpcija broj 2 - Rad sa kulturnim manifestacijama");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

}
