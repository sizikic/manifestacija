package ui;

import java.util.List;
import dao.GradDAO;
import model.Grad;
import utils.ScannerWrapper;
import java.sql.Connection;

public class GradUI {

	public static void menu() {

		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveGradove(ApplicationUI.conn);
				break;
			case 2:
				unesiGrad();
				break;
			case 3:
				izmeniGrad();
				break;
			case 4:
				obrisiGrad();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa gradovima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis podataka o svim gradovima");
		System.out.println("\tOpcija broj 2 - unesi nov grad");
		System.out.println("\tOpcija broj 3 - izmeni grad");
		System.out.println("\tOpcija broj 4 - obriši grad");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	protected static void ispisiSveGradove(Connection conn) {
		List<Grad> gradovi = GradDAO.getAll(ApplicationUI.conn);
		for (Grad g : gradovi) {
			System.out.println(g);
		}
	}

	protected static Grad pronadjiGradId(int id) {
		Grad retVal = GradDAO.getGradById(id, ApplicationUI.conn);
		return retVal;
	}

	protected static Grad pronadjiGradPtt(int ptt) {
		Grad retVal = GradDAO.getGradByPtt(ptt, ApplicationUI.conn);
		return retVal;
	}

	protected static Grad pronadjiGrad() {
		System.out.println("Unesite ptt broj grada:");
		int ptt = ScannerWrapper.ocitajCeoBroj();
		Grad retVal = GradDAO.getGradByPtt(ptt, ApplicationUI.conn);
		return retVal;
	}

	protected static boolean unesiGrad() {
		System.out.println("Unesi ptt broj novog grada:");
		int ptt = ScannerWrapper.ocitajCeoBroj();
		Grad grad = GradDAO.getGradByPtt(ptt, ApplicationUI.conn);
		while (grad != null) {
			System.out.println("Grad sa unetim ptt brojem već postoji u evidenciji!");
			System.out.println("Unesi ptt broj novog grada:");
			ptt = ScannerWrapper.ocitajCeoBroj();
			grad = GradDAO.getGradByPtt(ptt, ApplicationUI.conn);
		}
		System.out.println("Unesi naziv grada:");
		String naziv = ScannerWrapper.ocitajTekst();
		grad = new Grad(0, naziv, ptt);
		boolean status = GradDAO.add(ApplicationUI.conn, grad);
		if (status) {
			System.out.println("Grad je uspeŠno dodat");
		}
		return status;
	}

	protected static Grad izmeniGrad() {
		Grad grad = pronadjiGrad();
		while (grad == null) {
			System.out.println("Grad sa unetim ptt brojem ne postoji u evidenciji!");
			grad = pronadjiGrad();
		}
		System.out.println("Unesi novo ime grada:");
		String naziv = ScannerWrapper.ocitajTekst();
		grad.setNaziv(naziv);
		System.out.println("Unesi nov ptt broj grada:");
		int ptt = ScannerWrapper.ocitajCeoBroj();
		grad.setPtt(ptt);
		GradDAO.update(ApplicationUI.conn, grad);
		return grad;
	}

	protected static void obrisiGrad() {
		Grad grad = pronadjiGrad();
		boolean status = false;
		if (grad != null) {
			status = GradDAO.delete(ApplicationUI.conn, grad.getGrad_id());
		}

		if (status) {
			System.out.println("Grad je uspešno obrisan!");
		}
	}

}
