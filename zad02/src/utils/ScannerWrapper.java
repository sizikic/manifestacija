package utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//U ovoj klasi su obrade izuzetaka, izuzeci se mogu propagirati i negde drugde, 
//ali je ovde pokusaj da se odmah oporavi od greske, odnosno nudi se korisniku da proba ponovo
public class ScannerWrapper {
	
	static Scanner sc = new Scanner(System.in);
	static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.");
	
	//citanje promenljive String
	public static String ocitajTekst(){
		String tekst = "";
		while(tekst == null || tekst.equals(""))
			tekst = sc.nextLine();
		
		return tekst;
	}
		
	//citanje promenljive integer
	public static int ocitajCeoBroj(){
		int ceoBroj = 0;
		boolean ocitan = false;
		while(ocitan == false) {
			try {
				ceoBroj = sc.nextInt();
				sc.nextLine();
				ocitan = true;
			} catch (Exception e) {
				System.out.println("GRESKA - Pogresno unsesena vrednost za ceo broj, pokusajte ponovo: ");
				sc.nextLine();
			}
		}
		return ceoBroj;
	}
		
	//citanje promenljive double
	public static float ocitajRealanBroj(){
		float realanBroj = 0;
		boolean ocitan = false;
		while(ocitan == false) {
			try {
				realanBroj = sc.nextFloat();
				sc.nextLine();
				ocitan = true;
			} catch (Exception e) {
				System.out.println("GRESKA - Pogresno unsesena vrednost za realan broj, pokusajte ponovo: ");
				sc.nextLine();
			}
		}
		return realanBroj;
	}
		
	//citanje promenljive char
	public static char ocitajKarakter(){
		char karakter = ' ';
		boolean ocitan = false;
		while(ocitan == false) {
			try {
				String text = sc.next();
				karakter = text.charAt(0);
				ocitan = true;
			} catch (Exception e) {
				System.out.println("GRESKA - Pogresno unsesena vrednost za karakter, pokusajte ponovo: ");
				sc.nextLine();
			}
		}
		return karakter;
	}
	
	//citanje promenljive char
	public static char ocitajOdlukuOPotvrdi(String tekst){
		System.out.println("Da li zelite " + tekst + " [Y/N]:");
		char odluka = ' ';
		while( !(odluka == 'Y' || odluka == 'N') ){
			odluka = ocitajKarakter();
			if (!(odluka == 'Y' || odluka == 'N')) {
				System.out.println("Opcije su Y ili N");
			}
		}
		return odluka;
	}
	
	public static Date ocitajDatum() {
		String datum = null;
		Date date = null;
		while (datum == null || datum.equals("")) {
			datum = sc.nextLine();
			try {
				sdf.parse(datum);
				date = sdf.parse(datum);
			} catch (Exception e) {
				System.out.println("Datum je u pogrešnom formatu!");
				date = null;
			}
		}
		return date;
	}
	
	public static java.sql.Date ocitajDatumSql() {
		String datum = null;
		Date date = null;
		while (datum == null || datum.equals("")) {
			datum = sc.nextLine();
			try {
				sdf.parse(datum);
				date = sdf.parse(datum);
			} catch (Exception e) {
				System.out.println("Datum je u pogrešnom formatu!");
				date = null;
			}
		}
		java.sql.Date dateSql = new java.sql.Date (date.getTime());
		return dateSql;
	}
	
	
	
}
