package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Grad;
import model.Km;
import ui.ApplicationUI;


public class KmDAO {

	public static List<Km> getAll(Connection conn) {
		List<Km> km = new ArrayList<Km>();
		try {
			String query = "SELECT * FROM km";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			while (rset.next()) {
				int km_id = rset.getInt(1);
				String naziv = rset.getString(2);
				int brojPosetilaca = rset.getInt(3);
				int grad_id = rset.getInt(4);
				Grad g = GradDAO.getGradById(grad_id, ApplicationUI.conn);
				km.add(new Km(km_id, naziv, brojPosetilaca, g));
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return km;
	}

	public static Km getKmById(int id, Connection conn) {
		Km retVal = null;
		try {
			String query = "SELECT km_id, naziv, broj_posetilaca, grad_id FROM km where km_id = " + id;
			PreparedStatement pstmt = ApplicationUI.conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			if (rset.next()) {
				int km_id = rset.getInt(1);
				String naziv = rset.getString(2);
				int posetioci = rset.getInt(3);
				int grad_id = rset.getInt(4);
				Grad g = GradDAO.getGradById(grad_id, conn);
				retVal = new Km(km_id, naziv, posetioci, g);
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return retVal;
	}

	public static Km getKmByName(String naziv, Connection conn) {
		Km retVal = null;
		try {
			String query = "SELECT km_id, naziv, broj_posetilaca, grad_id FROM km where naziv like '%" + naziv + "%'";
			PreparedStatement pstmt = ApplicationUI.conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			if (rset.next()) {
				int km_id = rset.getInt(1);
				String nazivKm = rset.getString(2);
				int posetioci = rset.getInt(3);
				int grad_id = rset.getInt(4);
				Grad g = GradDAO.getGradById(grad_id, conn);
				retVal = new Km(km_id, nazivKm, posetioci, g);
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return retVal;
	}

	public static boolean add(Connection conn, Km km) {
		boolean status = false;
		try {
			String insert = "INSERT INTO km (naziv, broj_posetilaca, grad_id) VALUES (?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(insert);
			pstmt.setString(1, km.getNaziv());
			pstmt.setInt(2, km.getBrojPosetilaca());
			pstmt.setInt(3, km.getGrad().getGrad_id());
			if (pstmt.executeUpdate() == 1) {
				status = true;
			}
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean update(Connection conn, Km km) {
		boolean status = false;
		try {
			String update = "UPDATE km SET naziv = ?, broj_posetilaca = ?, grad_id = ? WHERE km_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, km.getNaziv());
			pstmt.setInt(2, km.getBrojPosetilaca());
			pstmt.setInt(3, km.getGrad().getGrad_id());
			pstmt.setInt(4, km.getKm_id());
			if (pstmt.executeUpdate() == 1) {
				status = true;
			}
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean delete(Connection conn, int id) {
		boolean status = false;
		try {
			String updateDelete = "DELETE FROM km WHERE km_id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(updateDelete) == 1) {
				status = true;
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static Km pronadjiKmMax(Connection conn) {
		Km km = new Km();
		try {
			String query = "select naziv, broj_posetilaca, grad_id from km where broj_posetilaca = (SELECT max(broj_posetilaca) FROM km)";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			if (rset.next()) {
				String naziv = rset.getString(1);
				int brojPosetilaca = rset.getInt(2);
				int grad_id = rset.getInt(3);
				Grad g = GradDAO.getGradById(grad_id, ApplicationUI.conn);
				km = new Km(0, naziv, brojPosetilaca, g);
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return km;
	}

}
