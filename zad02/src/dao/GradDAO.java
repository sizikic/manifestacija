package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Grad;
import ui.ApplicationUI;

public class GradDAO {

	public static List<Grad> getAll(Connection conn) {
		List<Grad> retVal = new ArrayList<Grad>();
		try {
			String query = "SELECT grad_id, naziv, ptt FROM grad";
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			while (rset.next()) {
				int grad_id = rset.getInt(1);
				String naziv = rset.getString(2);
				int ptt = rset.getInt(3);
				retVal.add(new Grad(grad_id, naziv, ptt));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return retVal;
	}

	public static Grad getGradById(int id, Connection conn) {
		Grad retVal = null;
		try {
			String query = "SELECT grad_id, naziv, ptt FROM grad where grad_id = " + id;
			PreparedStatement pstmt = ApplicationUI.conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			if (rset.next()) {
				int grad_id = rset.getInt(1);
				String naziv = rset.getString(2);
				int ptt = rset.getInt(3);
				retVal = new Grad(grad_id, naziv, ptt);
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return retVal;
	}

	public static Grad getGradByPtt(int ptt, Connection conn) {
		Grad retVal = null;
		try {
			String query = "SELECT grad_id, naziv, ptt FROM grad where ptt = " + ptt;
			PreparedStatement pstmt = ApplicationUI.conn.prepareStatement(query);
			ResultSet rset = pstmt.executeQuery();
			if (rset.next()) {
				int grad_id = rset.getInt(1);
				String naziv = rset.getString(2);
				int pttG = rset.getInt(3);
				retVal = new Grad(grad_id, naziv, pttG);
			}
		} catch (SQLException e) {
			System.out.println("Greska sa sql-om.");
		}
		return retVal;
	}

	public static boolean add(Connection conn, Grad grad) {
		boolean status = false;
		try {
			String insert = "INSERT INTO grad (naziv, ptt) VALUES (?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(insert);
			pstmt.setString(1, grad.getNaziv());
			pstmt.setInt(2, grad.getPtt());
			if (pstmt.executeUpdate() == 1) {
				status = true;
			}
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean update(Connection conn, Grad grad) {
		boolean status = false;
		try {
			String update = "UPDATE grad SET naziv = ?, ptt = ? WHERE grad_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, grad.getNaziv());
			pstmt.setInt(2, grad.getPtt());
			pstmt.setInt(3, grad.getGrad_id());
			if (pstmt.executeUpdate() == 1) {
				status = true;
			}
			pstmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	public static boolean delete(Connection conn, int id) {
		boolean status = false;
		try {
			String updateDelete = "DELETE FROM grad WHERE grad_id = " + id;
			Statement stmt = conn.createStatement();
			if (stmt.executeUpdate(updateDelete) == 1) {
				status = true;
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return status;

	}

}
